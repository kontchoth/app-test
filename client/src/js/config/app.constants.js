const AppConstants = {
  api: 'http://localhost:3000/api',
  jwtKey: 'jwtToken',
  appName: 'Currency Convertor Based on USD'
};

export default AppConstants;
