class HomeCtrl {
  constructor(AppConstants, $http) {
    'ngInject';
    this._AppConstants = AppConstants;
    this.appName = AppConstants.appName;
    this._$http = $http;
    this.from = 'USD';
    this.to = 'PHP';
    this.convertCurrency();

  }

  convertCurrency() {
    return this._$http({
      method: 'GET',
      url: this._AppConstants.api + '/data',
      crossDomain: true,
    }).then(
      (res) => {this.results = this.parseResponse(res.data);},
      (err) => {this.errors = err.errors;}
    );
  }

  parseResponse(data) {
    let currencies = [];
    for (var key in data.rates) {
      currencies.push({
        key: key,
        value: data.rates[key]
      });
    }
    return currencies;
  }
}

export default HomeCtrl;
