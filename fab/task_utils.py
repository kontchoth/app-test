from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

from contextlib import contextmanager
import os
import platform
import sys
import time

from fabric.api import env, local, task, warn_only
from fabric.context_managers import hide, settings
from fabric.contrib import django
import json




# Setup colors.
try:
    from colorama import Back, Fore, Style, init
except ImportError:
    local("pip install colorama==0.2.5 Django==1.10.2")
    from colorama import Back, Fore, Style, init

init(autoreset=True)

IS_TTY = sys.stdout.isatty()
IS_WIN = platform.system().lower().startswith("win")
STATUS_MARK = u'\u2712' * IS_TTY
X_MARK = u'\u2718' * IS_TTY
CHECK_MARK = u'\u2714' * IS_TTY
WARNING_MARK = u"\u26A0" * IS_TTY
NOTE_MARK = u'\u2710' * IS_TTY

def W(string, prefix=" "):
    """Returns "" if this platform is WIN."""
    return "" if IS_WIN else prefix + string


def hide_warnings():
    return settings(hide('warnings'))


def _fprint(bg, status, message):
    print(Fore.WHITE + Style.BRIGHT + bg + " " + status + " ", end="")
    print(Fore.WHITE + Style.BRIGHT + " " + message)


def warn(message):
    _fprint(Back.YELLOW, "WARNING", message + W(Fore.YELLOW + WARNING_MARK))


def success(message):
    _fprint(Back.GREEN, "SUCCESS", message + W(Fore.GREEN + CHECK_MARK))


def note(message):
    _fprint(Back.CYAN, " NOTE  ", message + W(Fore.CYAN + NOTE_MARK))


def error(message):
    _fprint(Back.RED, " ERROR ", message + W(Fore.RED + X_MARK))


@contextmanager
def announce(what):
    """Status decorate an event."""
    start_time = time.time()
    announcer_dict = {}

    def timer():
        return " [%s]" % str(time.time() - start_time)

    print(Fore.WHITE + Back.MAGENTA + Style.BRIGHT + "  START  ", end="")
    print(Fore.WHITE + Style.BRIGHT + W(STATUS_MARK) + " " + what)
    try:
        yield announcer_dict
    except:
        error(what + timer())
        raise  # let fabric handle this.
    else:
        if not announcer_dict:
            success(what + timer())
        else:
            warn(what + timer())

