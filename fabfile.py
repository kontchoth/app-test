# Non-Launch task here.
from fab.task_utils import *

# constants here
SERVER = 'server'
CLIENT = 'client'

# Launch tasks here

@task(aliases=['requirements', 'r'])
def install_requirements(side):
	with announce('[BUILDER] - Installing Requirements'):
		local('cd {} && npm install'.format(side))

@task(aliases=['startServer', 'ss'])
def start_server():
	with announce('[BUILDER] - Building {}'.format(SERVER)):
		# install all modules
		cmd = 'nohup npm start > log.txt &'
		local('cd {} && {}'.format(SERVER, cmd))
		print('{} Started log can be found in {}/log.txt'.format(SERVER, SERVER))

@task(aliases=['startClient', 'sc'])
def start_client():
	with announce('[BUILDER] - Building {}'.format(CLIENT)):
		# install all modules
		cmd = 'nohup gulp > log.txt &'
		local('cd {} && {}'.format(CLIENT, cmd))
		print('{} Started log can be found in {}/log.txt'.format(CLIENT, CLIENT))

@task(aliases=['stopAll', 'stop'])
def stop_all():
	with announce('[BUILDER] - Stop All'):
		local('killall node')
		local('killall gulp')
	print('Press Enter to exit....')

@task(aliases=['startAll', 'start'])
def start_all():
	install_requirements(SERVER)
	start_server()

	install_requirements(CLIENT)
	start_client()
