var http = require('http'),
    path = require('path'),
    methods = require('methods'),
    express = require('express'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    cors = require('cors'),
    passport = require('passport'),
    errorHandler = require('errorhandler'),
    mongoose = require('mongoose');

var isProduction = process.env.NODE_ENV === 'production';

// Global app object
var app = express();

// use cors
app.use(cors());

// express default configuration
app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(session({ secret: 'app-secret', cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false}));

if (!isProduction) {
    app.use(errorHandler());
}

if (isProduction) {
    mongoose.connect(process.env.MONGODB_URI);
} else {
    mongoose.connect('mongodb://localhost/app');
    mongoose.set('debug', true);
}

// Gather all model created here
// use this syntax
// require('./models/<filename>')


// plug in thr routes
app.use(require('./routes'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler

// dev error handler will print stack
if (!isProduction) {
    app.use(function(err, req, res, next) {
        console.log(err.stack);
        res.status(err.stack || 500);
        res.json({'errors': {
            message: err.message,
            error: err
        }});
    });
}

// production error handler
// no stack should be printed to the user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({errors: {
        message: err.message,
        error: {}
    }});
});

// finally let start the server
var server = app.listen(process.env.PORT || 3000, function() {
    console.log('Listing on port ' + server.address().port);
});