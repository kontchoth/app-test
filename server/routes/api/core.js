var router = require('express').Router();
var auth = require('../auth');
var request = require('request');

router.get('/data', auth.optional, function(req, res, next) {
    request({
        uri: 'http://api.fixer.io/latest',
        json: true

    }, function (error, response, body) {
        if (!error) {
            return res.json(body);
        }
        return res.status(500).json(error);
      }
    );
});

module.exports = router;